package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import JavaBeans.Account;
import model.EncryptPass;

public class AccountDAO {
	private final String JDBC_URL = "jdbc:mysql://localhost/web_board";
	private final String DB_USER = "root";
	private final String DB_PASS = "F9NegIhU5PufR";
	private static final String DRIVER = "com.mysql.jdbc.Driver";

	static {
		try {
			Class.forName(DRIVER);
		}catch(ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public boolean editAccountInPass(Account account) {
		try(Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sqlOrder = "update users "
					+ "set login_id = ?, password = ?, name = ?, brach_id = ?, department_id = ? "
					+ "where id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);
			String password = EncryptPass.encrypt(account.getPassword());
			pStmt.setString(1, account.getLoginId());
			pStmt.setString(2, password);
			pStmt.setString(3, account.getName());
			pStmt.setInt(4, account.getBranch());
			pStmt.setInt(5, account.getDepartment());
			pStmt.setInt(6, account.getUserId());
			int result = pStmt.executeUpdate();

			if (result == 0) {
				return false;
			}

		}catch(SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean editAccount(Account account) {
		try(Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sqlOrder = "update users "
					+ "set login_id = ?, name = ?, brach_id = ?, department_id = ?, is_stopped = ? "
					+ "where id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);
			pStmt.setString(1, account.getLoginId());
			pStmt.setString(2, account.getName());
			pStmt.setInt(3, account.getBranch());
			pStmt.setInt(4, account.getDepartment());
			pStmt.setInt(5, account.getIsStopped());
			pStmt.setInt(6, account.getUserId());
			int result = pStmt.executeUpdate();

			if (result != 1) {
				return false;
			}

		}catch(SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean createAccount(Account newAccount) {
		try(Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sqlOrder = "insert into users (login_id, password, name, brach_id, department_id, is_stopped, registered_at)"
					+ "values(?, ?, ?, ?, ?, ?, current_timestamp)";
			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);
			pStmt.setString(1, newAccount.getLoginId());
			pStmt.setString(2, EncryptPass.encrypt(newAccount.getPassword()));
			pStmt.setString(3, newAccount.getName());
			pStmt.setInt(4, newAccount.getBranch());
			pStmt.setInt(5, newAccount.getDepartment());
			pStmt.setInt(6, newAccount.getIsStopped());
			int result = pStmt.executeUpdate();

			if (result != 1) {
				return false;
			}

		}catch(SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public int editedLoginIdIsOriginal(Account editedAccount) {
		int sameIdCount;
		try(Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sqlOrder = "select count(*) as count from users where id != ? and login_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);
			pStmt.setInt(1, editedAccount.getUserId());
			pStmt.setString(2,  editedAccount.getLoginId());
			ResultSet result = pStmt.executeQuery();
			result.next();
			sameIdCount = result.getInt("count");
		}catch(SQLException e) {
			e.printStackTrace();
			return 2;
		}
		return sameIdCount;
	}

	public int loginIdIsUnique(Account newAccount) {
		int sameIdCount;
		try(Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sqlOrder = "select count(*) as count from users where login_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);
			pStmt.setString(1, newAccount.getLoginId());
			ResultSet result = pStmt.executeQuery();
			result.next();
			sameIdCount = result.getInt("count");
		}catch(SQLException e) {
			e.printStackTrace();
			return 2; // 変えたい
		}
		return sameIdCount;
	}

	public String getPassword(Account loginAccount) {

		try(Connection conn =DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sqlOrder = "select password from users where login_id = ? and is_stopped = 0";
			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);

			pStmt.setString(1,loginAccount.getLoginId());

			ResultSet result = pStmt.executeQuery();
			result.next();
			return result.getString("password");

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public boolean getUserId(Account account) {
		try(Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sqlOrder = "select id from users where login_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);
			pStmt.setString(1,  account.getLoginId());
			ResultSet result = pStmt.executeQuery();
			result.next();
			account.setUsersTableId(result.getInt("id"));
		}catch(SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public ArrayList<Account> getAccountAll() {
		ArrayList<Account> accountAll = new ArrayList<>();
		try(Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sqlOrder = "select users.id, login_id, users.name, branches.name, departments.name, is_stopped from users"
					+ "  join branches on branches.id = users.brach_id "
					+ " join departments on departments.id = users.department_id order by users.id";
			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);
			ResultSet result = pStmt.executeQuery();
			while(result.next()) {
				int userId = result.getInt("users.id");
				String loginId = result.getString("login_id");
				String name = result.getString("users.name");
				int isStopped = result.getInt("is_stopped");
				String branchName = result.getString("branches.name");
				String departmentName = result.getString("departments.name");
				Account account = new Account(userId, loginId, name, branchName, departmentName, isStopped);
				accountAll.add(account);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
		return accountAll;
	}

	public Account getAccount(int id) {
		try(Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sqlOrder = "select * from users where id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);
			pStmt.setInt(1,  id);
			ResultSet result = pStmt.executeQuery();
			result.next();
			int userId = result.getInt("id");
			String loginId = result.getString("login_id");
			String name = result.getString("name");
			int isStopped = result.getInt("is_stopped");
			int branchId = result.getInt("brach_id");
			int departmentId = result.getInt("department_id");

			Account account = new Account(userId, loginId, name, branchId, departmentId, isStopped);
			return account;
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	// ログイン時に使うやつ
	public Integer getUserID(Account account) {
		try(Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sqlOrder = "select id from users where login_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);
			pStmt.setString(1, account.getLoginId());
			ResultSet result = pStmt.executeQuery();
			result.next();
			int userId = result.getInt("id");
			return userId;
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Integer getBranchId(int userId) {
		try(Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sqlOrder = "select brach_id from users where id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);
			pStmt.setInt(1, userId);
			ResultSet result = pStmt.executeQuery();
			result.next();
			int branchId = result.getInt("brach_id");
			return branchId;
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Integer getDepartmentId(int userId) {
		try(Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sqlOrder = "select department_id from users where id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);
			pStmt.setInt(1, userId);
			ResultSet result = pStmt.executeQuery();
			result.next();
			int departmentId = result.getInt("department_id");
			return departmentId;
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Integer getManagerDepartmentId() {
		try(Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sqlOrder = "select id from departments where name = ?";
			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);
			pStmt.setString(1, "総務人事");
			ResultSet result = pStmt.executeQuery();
			result.next();
			Integer departmentId = result.getInt("id");
			return departmentId;
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Integer getManagerBranchId() {
		try(Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sqlOrder = "select id from branches where name = ?";
			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);
			pStmt.setString(1,  "本社");
			ResultSet result = pStmt.executeQuery();
			result.next();
			Integer branchId = result.getInt("id");
			return branchId;
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void stopAccount(int id) {
		try(Connection conn=DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sqlOrder = "update users set is_stopped = 1 where id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);
			pStmt.setInt(1,  id);
			pStmt.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}


	public void activeAccount(int id) {
		try(Connection conn=DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sqlOrder = "update users set is_stopped = 0 where id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);
			pStmt.setInt(1,  id);
			pStmt.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
}