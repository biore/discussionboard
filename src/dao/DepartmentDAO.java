package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import JavaBeans.Department;

public class DepartmentDAO {
	private final String JDBC_URL = "jdbc:mysql://localhost/web_board";
	private final String DB_USER = "root";
	private final String DB_PASS = "F9NegIhU5PufR";
	private static final String DRIVER = "com.mysql.jdbc.Driver";

	static {
		try {
			Class.forName(DRIVER);
		}catch(ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}


	public ArrayList<Department> getAll(){
		ArrayList<Department> departmentAll = new ArrayList<>();
		try(Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sqlOrder = "select * from departments order by id";
			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);
			ResultSet result = pStmt.executeQuery();
			while(result.next()) {
				int id = result.getInt("id");
				String name = result.getString("name");
				Department department = new Department(id, name);
				departmentAll.add(department);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
		return departmentAll;
	}
}
