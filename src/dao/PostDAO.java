package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import JavaBeans.Account;
import JavaBeans.Comment;
import JavaBeans.Post;

public class PostDAO {
	private final String JDBC_URL = "jdbc:mysql://localhost/web_board";
	private final String DB_USER = "root";
	private final String DB_PASS = "F9NegIhU5PufR";
	private static final String DRIVER = "com.mysql.jdbc.Driver";

	static {
		try {
			Class.forName(DRIVER);
		}catch(ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public boolean createPost(Post post, HttpSession session) {
		try(Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sqlOrder = "insert into posts (user_id, posted_at, category"
					+ ", subject, post_content, is_deleted)"
					+ " values(?, current_timestamp, ?, ?, ?, ?);";
			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);
			Account loginAccount = (Account) session.getAttribute("loginAccount");
			AccountDAO accountDAO = new AccountDAO();
			accountDAO.getUserId(loginAccount);
			pStmt.setInt(1, loginAccount.getUsersTableId());
			pStmt.setString(2,  post.getCategory());
			pStmt.setString(3, post.getSubject());
			pStmt.setString(4, post.getContent());
			pStmt.setInt(5, post.getIsDeleted());
			int result = pStmt.executeUpdate();

			if (result != 1) {
				return false;
			}
		}catch(SQLException e) {
				e.printStackTrace();
				return false;
		}
		return true;
	}

	public ArrayList<Post> getPosts(String sqlOrder) {
		ArrayList<Post> homeScreenPosts = new ArrayList<>();
		try(Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){

			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);
			ResultSet resultPosts = pStmt.executeQuery();

			while(resultPosts.next()) {

				int userId = resultPosts.getInt("posts.user_id");
				String name = resultPosts.getString("name"); // postそれぞれのログインIDを表示するので間違え
				String subject = resultPosts.getString("subject");
				String content = resultPosts.getString("post_content");
				String category = resultPosts.getString("category");
				String postedAt = resultPosts.getString("posted_at");
				int postId = resultPosts.getInt("id");

				CommentDAO commentDAO = new CommentDAO();
				ArrayList<Comment> comments = commentDAO.getComments(postId);

				Post post = new Post(subject, content, category, name, postedAt, postId);
				post.setCommentList(comments);
				post.setUserId(userId);
				homeScreenPosts.add(post);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
		return homeScreenPosts;
	}



	public boolean deletePost(int postId) {
		try(Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sqlOrder = "update posts set is_deleted = ? where id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);
			pStmt.setInt(1, 1);
			pStmt.setInt(2,  postId);
			int result = pStmt.executeUpdate();

			if (result == 0) {
				return false;
			}
		}catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}
}

