package dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import JavaBeans.Comment;
import JavaBeans.Post;

public class CommentDAO {
	private final String JDBC_URL = "jdbc:mysql://localhost/web_board";
	private final String DB_USER = "root";
	private final String DB_PASS = "F9NegIhU5PufR";
	private static final String DRIVER = "com.mysql.jdbc.Driver";

	static {
		try {
			Class.forName(DRIVER);
		}catch(ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public ArrayList<Comment> getComments(int postId) {
		ArrayList<Comment> homeScreenComments = new ArrayList<>();
		try(Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sqlOrder = "select comments.id, comment_content, commented_at, users.name, comments.user_id from comments join users on users.id = comments.user_id "
					+ "join posts on posts.id = comments.post_id "
					+ "where posts.id = ? and comments.is_deleted = ? order by comments.id";
			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);
			pStmt.setInt(1,  postId);
			pStmt.setInt(2,  0);
			ResultSet result = pStmt.executeQuery();

			while(result.next()) {
				int commentId = result.getInt("comments.id");
				String commentContent = result.getString("comment_content");
				String commentedAt = result.getString("commented_at");
				String userName = result.getString("users.name");
				int userId = result.getInt("comments.user_id");
				Comment comment = new Comment();
				comment.setCommentedAt(commentedAt);
				comment.setCommentId(commentId);
				comment.setComment(commentContent);
				comment.setUserName(userName);
				comment.setUserId(userId);

				homeScreenComments.add(comment);
			}
		}catch(SQLException e) {
				e.printStackTrace();
				return null;
		}

		return homeScreenComments;
	}

	public boolean createComment(Post post) {
		try(Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sqlOrder = "insert into comments (post_id, user_id, comment_content, is_deleted,"
					+ "commented_at) "
					+ "values(?, ?, ?, ?, current_timestamp);";
			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);
			pStmt.setInt(1,  post.getPostId());
			pStmt.setInt(2, post.getUserId());
			pStmt.setString(3, post.getComment());
			pStmt.setInt(4, 0);

			int result = pStmt.executeUpdate();
			if(result!=1) {
				return false;
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean deleteComment(int commentId) {
		try(Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)){
			String sqlOrder = "update comments set is_deleted = ? where  id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sqlOrder);
			pStmt.setInt(1,  1);
			pStmt.setInt(2,  commentId);

			int result = pStmt.executeUpdate();
			if(result == 0) {
				return false;
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}
}
