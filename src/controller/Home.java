package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JavaBeans.Account;
import JavaBeans.Post;
import model.CreateComment;
import model.DeleteComment;
import model.DeletePost;
import model.DisplayPosts;
import model.GetToday;

/**
 * Servlet implementation class Home
 */
@WebServlet("/index.jsp")
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */

	public Home() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		HttpSession session = request.getSession();
		Account loginAccount = (Account) session.getAttribute("loginAccount");
		if (loginAccount == null) {
			response.sendRedirect("login");
		} else {
			String action = request.getParameter("action");

			if (action != null) {
				if (action.equals("notManager")) {
					String message = "管理者権限を持っていないため、管理機能は使えません。";
					request.setAttribute("message", message);
				} else if (action.equals("commentError")) {
					String message = "コメントを入力してください";
					request.setAttribute("message", message);
				}
			}
			if (action == null) {
				int year = GetToday.todaysYear();
				int month = GetToday.todaysMonth();
				int day = GetToday.todaysDay();

				request.setAttribute("year", year);
				request.setAttribute("month", month);
				request.setAttribute("day", day);
			}
			String sqlOrder = "select * from posts  join users on  users.id = posts.user_id where posts.is_deleted = 0 order by posts.id desc";
			ArrayList<Post> homePosts = DisplayPosts.displayPosts(sqlOrder);
			session.setAttribute("homePosts", homePosts);
			request.getRequestDispatcher("/WEB-INF/jsp/home.jsp").forward(request, response);

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		String action = request.getParameter("action");

		HttpSession session = request.getSession();
		Account loginAccount = (Account) session.getAttribute("loginAccount");

		if (action.equals("comment")) {
			int id = Integer.parseInt(request.getParameter("id"));
			String comment = request.getParameter("comment");
			if (comment.isEmpty()) {
				response.sendRedirect("index.jsp?action=commentError");
			} else {

				Post post = new Post();
				post.setComment(comment);
				post.setPostId(id);
				post.setUserId(loginAccount.getUserId());
				CreateComment.createComment(post);
				response.sendRedirect("index.jsp");
			}
		} else if (action.equals("deletePost")) {
			int id = Integer.parseInt(request.getParameter("id"));
			DeletePost.deletePost(id);
			response.sendRedirect("index.jsp");
		} else if (action.equals("deleteComment")) {
			int id = Integer.parseInt(request.getParameter("id"));
			DeleteComment.deleteComment(id);
			response.sendRedirect("index.jsp");
		} else {
			int year = GetToday.todaysYear();
			int month = GetToday.todaysMonth();
			int day = GetToday.todaysDay();

			request.setAttribute("year", year);
			request.setAttribute("month", month);
			request.setAttribute("day", day);

			String category = request.getParameter("category");
			if (action.equals("searchCategory") && !(category.isEmpty())) {

				request.setAttribute("category", category);

				String sqlOrder = "select * from posts join users on  users.id = posts.user_id where posts.is_deleted = 0 and posts.category like "
						+ "\'%"+category+"%\'" + " order by posts.id desc";
				ArrayList<Post> homePosts = DisplayPosts.displayPosts(sqlOrder);
				session.setAttribute("homePosts", homePosts);
			} else if (action.equals("searchDate")) {
				year = Integer.parseInt(request.getParameter("year"));
				month = Integer.parseInt(request.getParameter("month"));
				day = Integer.parseInt(request.getParameter("day"));
				request.setAttribute("year", year);
				request.setAttribute("month", month);
				request.setAttribute("day", day);
				String date = String.format("%04d", year) + "-" + String.format("%02d", month) + "-"
						+ String.format("%02d", day);
				String dateAfterADay = String.format("%04d", year) + "-" + String.format("%02d", month) + "-"
						+ String.format("%02d", day+1);
				String sqlOrder = "select * from posts join users on  users.id = posts.user_id where posts.is_deleted = 0 and posts.posted_at >= \'"
						+ date + "\' and posts.posted_at <=  \'" + dateAfterADay + "\'  order by posts.id desc";
				ArrayList<Post> homePosts = DisplayPosts.displayPosts(sqlOrder);
				session.setAttribute("homePosts", homePosts);
			} else {
				String sqlOrder = "select * from posts  join users on  users.id = posts.user_id where posts.is_deleted = 0 order by posts.id desc";
				ArrayList<Post> homePosts = DisplayPosts.displayPosts(sqlOrder);
				session.setAttribute("homePosts", homePosts);
			}
			request.getRequestDispatcher("/WEB-INF/jsp/home.jsp").forward(request, response);

		}
	}
}
