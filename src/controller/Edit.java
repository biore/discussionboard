package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JavaBeans.Account;
import JavaBeans.Branch;
import JavaBeans.Department;
import dao.AccountDAO;
import model.EditAccount;
import model.EditCheck;
import model.GetBranchAll;
import model.GetDepartmentAll;
import model.IsManager;

/**
 * Servlet implementation class Edit
 */
@WebServlet("/edit")
public class Edit extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Edit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		String action = (String) request.getParameter("action");
		HttpSession session = request.getSession();
		Account loginAccount = (Account) session.getAttribute("loginAccount");
		if(loginAccount == null) {
			response.sendRedirect("login");
		}else{
			boolean isManager = IsManager.isManager(loginAccount);
			if(!(isManager)) {
				response.sendRedirect("index.jsp?action='notManager'");
			}else {
				if (action == null) {
					ArrayList<Branch> branchAll = GetBranchAll.getBranchAll();
					ArrayList<Department> departmentAll = GetDepartmentAll.getDepartmentAll();
					request.setAttribute("branchAll", branchAll);
					request.setAttribute("departmentAll", departmentAll);
					int userId = Integer.parseInt(request.getParameter("userId"));
					AccountDAO accountDAO = new AccountDAO();
					Account editedAccount = accountDAO.getAccount(userId);
					session.setAttribute("editedAccount", editedAccount);
					request.getRequestDispatcher("WEB-INF/jsp/edit.jsp").forward(request, response);
				}else{
					session.removeAttribute("editedAccount");
					response.sendRedirect("ManagerScreen");
				}
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		Account editedAccount = (Account) session.getAttribute("editedAccount");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		String name = request.getParameter("name");
		int branch;
		int department;
		branch = Integer.parseInt(request.getParameter("branch"));
		department =Integer.parseInt(request.getParameter("department"));


		editedAccount.setErrorMessage("");
		editedAccount.setLoginId(loginId);
		editedAccount.setPassword(password);
		editedAccount.setPasswordConfirm(passwordConfirm);
		editedAccount.setName(name);
		editedAccount.setBranch(branch);
		editedAccount.setDepartment(department);
		System.out.println(password.isEmpty());
		EditCheck.editCheck(editedAccount);
		boolean correctSubscribe = (editedAccount.getErrorMessage().length() == 0);
		if(correctSubscribe) {
			// データベースに登録
			if(password.isEmpty() && passwordConfirm.isEmpty()) {
				EditAccount.editAccount(editedAccount);
			}else {
				EditAccount.editAccountInPass(editedAccount);
			}
			session.removeAttribute("editedAccount");
			response.sendRedirect("ManagerScreen");
		}else {
			ArrayList<Branch> branchAll = GetBranchAll.getBranchAll();
			ArrayList<Department> departmentAll = GetDepartmentAll.getDepartmentAll();
			session.setAttribute("editedAccount", editedAccount);
			request.setAttribute("branchAll", branchAll);
			request.setAttribute("departmentAll", departmentAll);
			request.getRequestDispatcher("/WEB-INF/jsp/edit.jsp").forward(request, response);
		}
	}
}
