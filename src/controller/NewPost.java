package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JavaBeans.Account;
import JavaBeans.Post;
import model.CreatePost;
import model.PostCheck;

/**
 * Servlet implementation class NewPost
 */
@WebServlet("/NewPost")
public class NewPost extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewPost() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		Account loginAccount = (Account) session.getAttribute("loginAccount");
		if (loginAccount == null) {
			response.sendRedirect("login");
		}else {
		request.getRequestDispatcher("/WEB-INF/jsp/newPost.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		Account loginAccount = (Account) session.getAttribute("loginAccount");
		String loginId = loginAccount.getLoginId();
		String subject = request.getParameter("subject");
		String content = request.getParameter("content");
		String category = request.getParameter("category");
		Post post = new Post(subject, content, category, loginId);
		boolean postIsValid = PostCheck.postCheck(post);
		if(postIsValid) {
			boolean postIsCreated = CreatePost.createPost(post,  session);
			if(postIsCreated) {
				response.sendRedirect("index.jsp");
			}else {
				request.setAttribute("post", post);
				request.getRequestDispatcher("/WEB-INF/jsp/newPost.jsp").forward(request, response);
			}

		}else {
			request.setAttribute("post", post);
			request.getRequestDispatcher("/WEB-INF/jsp/newPost.jsp").forward(request, response);
		}

	}
}

