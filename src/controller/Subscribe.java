package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JavaBeans.Account;
import JavaBeans.Branch;
import JavaBeans.Department;
import model.CreateAccount;
import model.GetBranchAll;
import model.GetDepartmentAll;
import model.IsManager;
import model.SubscribeChecker;

/**
 * Servlet implementation class Subscribe
 */
@WebServlet("/subscribe")
public class Subscribe extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Subscribe() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		Account loginAccount = (Account) session.getAttribute("loginAccount");
		if(loginAccount == null) {
			response.sendRedirect("login");
		}else{
			boolean isManager = IsManager.isManager(loginAccount);
			if(!(isManager)) {
				response.sendRedirect("index.jsp?action='notManager'");
			}else {
				ArrayList<Branch> branchAll = GetBranchAll.getBranchAll();
				ArrayList<Department> departmentAll = GetDepartmentAll.getDepartmentAll();

				request.setAttribute("branchAll", branchAll);
				request.setAttribute("departmentAll", departmentAll);
				request.getRequestDispatcher("/WEB-INF/jsp/subscribe.jsp").forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		String name = request.getParameter("name");
		int branch;
		int department;
		String errorMessage = "";
		try {
			branch = Integer.parseInt(request.getParameter("branch"));
			department =Integer.parseInt(request.getParameter("department"));
		}catch(NumberFormatException e) {
			branch = 0;
			department = 0;
			errorMessage += "支店または部署が未指定です<br>";
		}
		Account newAccount = new Account(loginId, password, passwordConfirm, name, branch, department);
		SubscribeChecker.subscribeCheck(newAccount);
		newAccount.addErrorMessage(errorMessage);
		boolean correctSubscribe = (newAccount.getErrorMessage().length() == 0);
		if(correctSubscribe) {
			// データベースに登録
			CreateAccount.createAccount(newAccount);
			response.sendRedirect("/DiscussionBoard/ManagerScreen");
		}else {
			request.setAttribute("newAccount", newAccount);
			ArrayList<Branch> branchAll = GetBranchAll.getBranchAll();
			ArrayList<Department> departmentAll = GetDepartmentAll.getDepartmentAll();
			request.setAttribute("branchAll", branchAll);
			request.setAttribute("departmentAll", departmentAll);
			request.getRequestDispatcher("/WEB-INF/jsp/subscribe.jsp").forward(request, response);
		}
	}

}
