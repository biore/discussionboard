package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JavaBeans.Account;
import model.ChangeAccountState;
import model.GetAccountAll;
import model.IsManager;

/**
 * Servlet implementation class ManagerScreen
 */
@WebServlet("/ManagerScreen")
public class ManagerScreen extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ManagerScreen() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		Account loginAccount = (Account) session.getAttribute("loginAccount");
		if(loginAccount == null) {
			response.sendRedirect("login");
		}else {
			boolean isManager = IsManager.isManager(loginAccount);
			if(!(isManager)) {
				response.sendRedirect("index.jsp?action='notManager'");
			}else {
				String action = request.getParameter("action");
				if(action != null) {
					int id = Integer.parseInt(request.getParameter("id"));
					if(action.equals("stop")) {
						ChangeAccountState.stopAccount(id);
					}else if(action.equals("active")) {
						ChangeAccountState.activeAccount(id);
					}
				}
				ArrayList<Account> accountAll = GetAccountAll.getAccountAll();
				request.setAttribute("accountAll", accountAll);
				request.getRequestDispatcher("/WEB-INF/jsp/managerScreen.jsp").forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
