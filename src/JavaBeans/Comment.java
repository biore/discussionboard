package JavaBeans;
import java.io.Serializable;
public class Comment implements Serializable{
	private String comment;
	private int commentId;
	private String postName;
	private String userName;
	private int isDeleted;
	private int userId;
	private String commentedAt;
	private int postId;

	public Comment() {}

	public Comment(String comment, int commentId, String postName, String userName, int isDeleted) {
		this.comment = comment;
		this.commentId = commentId;
		this.postName = postName;
		this.userName = userName;
		this.isDeleted = isDeleted;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getCommentId() {
		return commentId;
	}

	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}

	public String getPostName() {
		return postName;
	}

	public void setPostName(String postName) {
		this.postName = postName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getCommentedAt() {
		return commentedAt;
	}

	public void setCommentedAt(String commentedAt) {
		this.commentedAt = commentedAt;
	}

	public int getPostId() {
		return postId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}
}
