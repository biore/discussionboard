package JavaBeans;
import java.io.Serializable;

public class Account implements Serializable {
	private String loginId;
	private String password;
	private String passwordConfirm;
	private String name;
	private int branch;
	private int department;
	private String errorMessage;
	private int isStopped;
	private int usersTableId;
	private String branchName;
	private String departmentName;
	private int userId;

	public Account(){
		this.loginId = "";
		this.password = "";
		this.passwordConfirm = "";
		this.name = "";
		this.branch = 0;
		this.department = 0;
		this.errorMessage = "";
		this.isStopped = 0;
	}

	public Account(String loginId, String password) {
		this.loginId = loginId;
		this.password = password;
		this.passwordConfirm = "";
		this.name = "";
		this.branch = 0;
		this.department = 0;
		this.errorMessage = "";
		this.isStopped = 0;
	}
	public Account(String loginId, String password, String passwordConfirm, String name, int branch, int department){
		this.loginId = loginId;
		this.password = password;
		this.passwordConfirm = passwordConfirm;
		this.name = name;
		this.branch = branch;
		this.department = department;
		this.errorMessage = "";
		this.isStopped =0;
	}
	public Account(int userId, String loginId, String name, int branch, int department, int isStopped) {
		this.userId = userId;
		this.loginId = loginId;
		this.name = name;
		this.branch = branch;
		this.department = department;
		this.isStopped = isStopped;
	}

	public Account(int userId, String loginId, String name, String branchName, String departmentName, int isStopped) {
		this.loginId = loginId;
		this.userId = userId;
		this.name = name;
		this.setBranchName(branchName);
		this.setDepartmentName(departmentName);
		this.isStopped = isStopped;
	}

	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPasswordConfirm() {
		return passwordConfirm;
	}
	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBranch() {
		return branch;
	}
	public void setBranch(int branch) {
		this.branch = branch;
	}
	public int getDepartment() {
		return department;
	}
	public void setDepartment(int department) {
		this.department = department;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getErrorMessage() {
		return errorMessage;
	}

	public void addErrorMessage(String errorMessage) {
		this.errorMessage += errorMessage;
	}

	public void passwordReset() {
		this.setPassword("");
		this.setPasswordConfirm("");
	}

	public int getIsStopped() {
		return isStopped;
	}
	public void setIsStopped(int isStopped) {
		this.isStopped = isStopped;
	}

	public int getUsersTableId() {
		return usersTableId;
	}

	public void setUsersTableId(int usersTableId) {
		this.usersTableId = usersTableId;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
}
