package JavaBeans;
import java.io.Serializable;
import java.util.ArrayList;

public class Post implements Serializable {
	private String subject;
	private String content;
	private String category;
	private String errorMessage;
	private int isDeleted = 0;
	private String loginId;
	private String userName;
	private String postedAt;
	private ArrayList<Comment> commentList;
	private int postId;
	private int userId;
	private String comment;

	public Post(){
		this.subject = "";
		this.content = "";
		this.category = "";
		this.errorMessage = "";
	}


	public Post(String subject, String content, String category){
		this.subject = subject;
		this.content = content;
		this.category = category;
		this.errorMessage = "";
	}

	public Post(String subject, String content, String category, String loginId) {
		this.subject = subject;
		this.content = content;
		this.category = category;
		this.loginId = loginId;
		this.errorMessage = "";
	}

	public Post(String subject, String content, String category, String userName, String postedAt, int postId) {
		this(subject, content, category);
		this.userName = userName;
		this.postedAt = postedAt;
		this.errorMessage = "";
		this.setPostId(postId);
	}

	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void addErrorMessage(String errorMessage) {
		this.errorMessage += errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}


	public int getIsDeleted() {
		return isDeleted;
	}


	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}


	public String getLoginId() {
		return loginId;
	}


	public void setLoginId(String userId) {
		this.loginId = userId;
	}


	public String getPostedAt() {
		return postedAt;
	}


	public void setPostedAt(String postedAt) {
		this.postedAt = postedAt;
	}


	public ArrayList<Comment> getCommentList() {
		return commentList;
	}


	public void setCommentList(ArrayList<Comment> commentList) {
		this.commentList = commentList;
	}


	public int getPostId() {
		return postId;
	}


	public void setPostId(int postId) {
		this.postId = postId;
	}


	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}


	public String getComment() {
		return comment;
	}


	public void setComment(String comment) {
		this.comment = comment;
	}

}
