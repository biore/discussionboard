package model;

import javax.servlet.http.HttpSession;

import JavaBeans.Post;
import dao.PostDAO;

public class CreatePost {
	public static boolean createPost(Post post, HttpSession session) {
		PostDAO postDAO = new PostDAO();
		boolean postIsCreated = postDAO.createPost(post, session);
		return postIsCreated;
	}
}
