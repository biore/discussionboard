package model;

import JavaBeans.Account;
import dao.AccountDAO;

public class IsManager {
	public static boolean isManager(Account account) {
		AccountDAO accountDAO = new AccountDAO();
		Integer userBranchId = accountDAO.getBranchId(account.getUserId());
		Integer userDepartmentId = accountDAO.getDepartmentId(account.getUserId());
		Integer managerBranchId = accountDAO.getManagerBranchId();
		Integer managerDepartmentId = accountDAO.getManagerDepartmentId();
		if(userBranchId == managerBranchId && userDepartmentId == managerDepartmentId) {
			return true;
		}else {
			return false;
		}
	}
}
