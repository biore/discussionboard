package model;

import java.util.ArrayList;

import JavaBeans.Post;
import dao.PostDAO;

public class DisplayPosts {
	public static ArrayList<Post> displayPosts(String sqlOrder){
		PostDAO postDAO = new PostDAO();
		ArrayList<Post> homeScreenPosts = postDAO.getPosts(sqlOrder);
		return homeScreenPosts;
	}

}
