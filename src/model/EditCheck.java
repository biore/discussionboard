package model;

import JavaBeans.Account;
import dao.AccountDAO;

public class EditCheck {
	public static void editCheck(Account account) {
		String errorMessage = "";
		// loginId の存在チェック
		if(!(account.getLoginId().matches("^[a-zA-Z0-9]{6,20}$"))) {
			errorMessage += "ログインIDを正しく入力してください（半角英数字　6～20文字）<br>";
		}else if(!(editedIdIsOrginal(account))) {
			errorMessage += "このログインIDは既に存在します";
			account.setLoginId("");
		}

		if(!(account.getPassword().isEmpty() && account.getPasswordConfirm().isEmpty())) {
			if(!(account.getPassword().equals(account.getPasswordConfirm()))) {
				errorMessage += "パスワードが一致しません<br>";
			}
			if(!(account.getPassword().matches("[ -~]{6,20}"))) {
				errorMessage += "パスワードを正しく入力してください（記号を含むすべての半角文字　6～20文字）<br>";
			}
		}

		if(!(account.getName().length() <= 10 && account.getName().length() >= 1)) {
			errorMessage += "名前を入力してください（10文字以内）<br>";
		}

		account.addErrorMessage(errorMessage);
	}

	public static boolean editedIdIsOrginal(Account account) {
		AccountDAO accountDAO = new AccountDAO();
		int sameIdCount = accountDAO.editedLoginIdIsOriginal(account);
		if(sameIdCount == 0) {
			return true;
		}else {
			return false;
		}

	}
}
