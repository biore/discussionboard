package model;

import java.util.ArrayList;

import JavaBeans.Branch;
import dao.BranchDAO;

public class GetBranchAll {
	public static ArrayList<Branch> getBranchAll(){
		BranchDAO branchDAO = new BranchDAO();
		return branchDAO.getAll();
	}
}
