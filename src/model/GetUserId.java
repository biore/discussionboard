package model;

import JavaBeans.Account;
import dao.AccountDAO;

public class GetUserId {
	public static void getUserId(Account account){
		AccountDAO accountDAO = new AccountDAO();
		Integer id = accountDAO.getUserID(account);
		account.setUserId(id);
	}
}
