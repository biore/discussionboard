package model;

import JavaBeans.Post;

public class PostCheck {
	public static boolean postCheck(Post post) {
		if(post.getSubject().length() <= 0  || post.getSubject().length() > 30) {
			post.addErrorMessage("件名を入力してください（30文字以内）<br>");
		}

		if(post.getContent().length() <= 0 || post.getContent().length() > 1000) {
			post.addErrorMessage("本文を入力してください（1000文字以内）<br>");
		}

		if(post.getCategory().length() <= 0 || post.getCategory().length() > 10) {
			post.addErrorMessage("カテゴリーを入力してください（10字以内）<br>");
		}

		if(post.getErrorMessage().length() == 0) {
			return true;
		}else {
			return false;
		}
	}
}
