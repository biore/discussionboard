package model;

import JavaBeans.Account;
import dao.AccountDAO;

public class EditAccount {
	public static void editAccount(Account account) {
		AccountDAO accountDAO = new AccountDAO();
		accountDAO.editAccount(account);
	}
	public static void  editAccountInPass(Account account) {
		AccountDAO accountDAO = new AccountDAO();
		accountDAO.editAccountInPass(account);
	}
}
