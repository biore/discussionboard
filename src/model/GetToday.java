package model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class GetToday {
	public static int todaysYear() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		int year = Integer.parseInt(sdf.format(date));
		return year;
	}

	public static int todaysMonth() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		int month = Integer.parseInt(sdf.format(date));
		return month;
	}

	public static int todaysDay() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd");
		int day = Integer.parseInt(sdf.format(date));
		return day;
	}
}
