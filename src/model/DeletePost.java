package model;

import dao.PostDAO;

public class DeletePost {
	public static void deletePost(int postId) {
		PostDAO postDAO = new PostDAO();
		postDAO.deletePost(postId);
	}
}
