package model;

import java.util.ArrayList;

import JavaBeans.Department;
import dao.DepartmentDAO;

public class GetDepartmentAll {
	public static ArrayList<Department> getDepartmentAll(){
		DepartmentDAO departmentDAO = new DepartmentDAO();
		return departmentDAO.getAll();
	}
}
