package model;

import JavaBeans.Post;
import dao.CommentDAO;

public class CreateComment {
	public static boolean createComment(Post post) {
		CommentDAO commentDAO = new CommentDAO();
		boolean resultCreateComment = commentDAO.createComment(post);
		return resultCreateComment;
	}
}
