package model;

import JavaBeans.Account;
import dao.AccountDAO;

public class LoginCheck {
	public static boolean loginCheck(Account loginAccount) {
		AccountDAO accountDAO = new AccountDAO();
		String password = accountDAO.getPassword(loginAccount);
		String inputPassword = EncryptPass.encrypt(loginAccount.getPassword());
		if(password == null || inputPassword == null) {
			return false;
		}
		if(password.equals(inputPassword)) {
			return true;
		}else {
			return false;
		}
	}
}
