package model;

import dao.CommentDAO;

public class DeleteComment {
	public static void deleteComment(int commentId) {
		CommentDAO commentDAO = new CommentDAO();
		commentDAO.deleteComment(commentId);
	}
}
