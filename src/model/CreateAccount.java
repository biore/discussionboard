package model;

import JavaBeans.Account;
import dao.AccountDAO;

public class CreateAccount {
	public static void createAccount(Account newAccount) {
		AccountDAO accountDAO = new AccountDAO();
		accountDAO.createAccount(newAccount);
	}
}
