package model;

import java.util.ArrayList;

import JavaBeans.Account;
import dao.AccountDAO;

public class GetAccountAll {
	public static ArrayList<Account> getAccountAll(){
		AccountDAO accountDAO = new AccountDAO();
		return accountDAO.getAccountAll();
	}
}
