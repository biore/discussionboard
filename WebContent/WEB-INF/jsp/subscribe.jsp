<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規登録画面</title>
</head>
<body>
	<form action="/DiscussionBoard/subscribe" method='post'>
		ログインID : <input type='text' name='loginId' value="${newAccount.loginId }"><br>
		パスワード : <input type='password' name='password'><br>
		パスワード（確認） : <input type='password' name='passwordConfirm'><br>
		名前 : <input type='text' name='name' value="${newAccount.name }"><br>

		支店 :
		<select name="branch">
			<option>未指定</option>
			<c:forEach var="branch" items="${branchAll}">
				<option value='${branch.id }'>${branch.name}</option>
			</c:forEach>
		</select><br>
		部署・役職 :
		<select name="department">
			<option>未指定</option>
			<c:forEach var="department" items="${departmentAll}">
				<option value= '${department.id}'>${department.name}</option>
			</c:forEach>
		</select><br>

		<input type='submit' value='登録'>
	</form>
	<br>
	<p>${newAccount.errorMessage} </p>
	<br>
	<%--↓url変える --%>
	<a href='ManagerScreen'>管理画面へ</a>

</body>
</html>