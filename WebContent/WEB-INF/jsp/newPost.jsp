<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規投稿画面</title>
</head>
<body>
	<form action="NewPost" method="post">
		件名 : <input type="text" name="subject" value="${post.subject }"><br>
		<br>
		<textarea name="content">${post.content }</textarea>
		<br><br>
		category : <input type='text' name='category' value="${post.category }"><br><br>
		<input type='submit' value='送信'><br>
	</form>
	<p>${post.errorMessage }</p>
	<br>
	<br>
	<br>
	<a href='index.jsp'>ホーム</a>
</body>
</html>