<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
</head>
<body>
	ログイン画面<br>
	<p>${logoutMessage }</p>
	<form action="login" method="post">
		ログインID : <input type='text' name='loginId'> <br>
		パスワード : <input type='password' name='password'> <br>
		<input type='submit' value='ログイン'><br>
	</form>
	<p>${errorMessage}</p>

</body>
</html>