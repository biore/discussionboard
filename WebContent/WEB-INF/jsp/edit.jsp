<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix='c' uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>編集画面</title>
</head>
<body>
	<form action="edit" method='post'>
		ログインID : <input type='text' name='loginId' value="${editedAccount.loginId }"><br>
		パスワード : <input type='password' name='password'><br>
		パスワード（確認） : <input type='password' name='passwordConfirm'><br>
		名前 : <input type='text' name='name' value="${editedAccount.name }"><br>

		支店 :
		<select name="branch">
			<c:forEach var="branch" items="${branchAll}">
				<option value=${branch.id } <c:if test='${editedAccount.branch == branch.id }'>selected</c:if>>
				${branch.name}</option>
			</c:forEach>
		</select><br>
		部署・役職 :
		<select name="department">
			<c:forEach var="department" items="${departmentAll}">
				<option value= "${department.id}" <c:if test='${editedAccount.department == department.id }'>selected</c:if>>
				${department.name}</option>
			</c:forEach>
		</select><br>

		<input type='submit' value='変更'>
	</form>
	<br>
	<br>
	<%--↓url変える --%>
	<a href='edit?action=true'>管理画面へ</a>

	<br><br>
	<p>${editedAccount.errorMessage }</p>


</body>
</html>