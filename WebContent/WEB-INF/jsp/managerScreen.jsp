<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>管理画面</title>
<link rel='stylesheet' href='managerScreen.css'>
</head>
<body>


	<p>管理画面です</p>
	<a class='subsc-btn' href="subscribe">新規登録画面</a>
	<a href="index.jsp">ホームへ</a>
	<br>
	<br>
	<br>
	<table border='1'>
		<tr>
			<th>id</th>
			<th>ログインID</th>
			<th>名前</th>
			<th>支店</th>
			<th>部署・役職</th>
			<th>active</th>
		</tr>


		<c:forEach var="account" items="${accountAll}">
			<tr>
				<td><a href='/DiscussionBoard/edit?userId=${account.userId }'>${account.userId }</a>
				<td>${account.loginId}</td>
				<td>${account.name }</td>
				<td>${account.branchName }</td>
				<td>${account.departmentName }</td>
				<td>
					<form id='${account.userId}'>
						<input type='radio' name='${account.userId }' value='${account.userId }' onclick="return active(${account.userId})"<c:if test='${account.isStopped == 0}'>checked='checked'</c:if>>
						<label for="active">有効</label>
						<input type='radio'name='${account.userId }' value='${account.userId }'  onclick="return stop(${account.userId})"<c:if test = '${account.isStopped == 1}'>checked='checked'</c:if>>
						<label for="stop">無効</label>
					</form>

				</td>
			</tr>
		</c:forEach>
	</table>

	<script type="text/javascript">
		function active(id) {
			var ans = window.confirm(id + "のアカウントを有効にしますか？");
			if (ans) {
				//var elements = element.(String(id));
				location.href="ManagerScreen?id=" + id + "&action=active";

			}else{
				return false;
			}
		}

		function stop(id){
			var ans = window.confirm(id + "アカウントを無効にしますか？");
			if(ans){

				location.href="ManagerScreen?id=" + id + "&action=stop";
			}else{
				return false;
			}
		}
	</script>


</body>
</html>