<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ホーム画面</title>
<link rel='stylesheet' href='homePosts.css'>
</head>
<body>
	<a href="/DiscussionBoard/logout">ログアウト</a>
	<br>
	<a href="/DiscussionBoard/ManagerScreen">管理画面へ</a>
	<br>
	<a href="/DiscussionBoard/NewPost">新規投稿</a>
	<br>
	<br>
	<form action='index.jsp?action=searchCategory' method='post'>
		カテゴリー
		<input type='search' name='category' placeholder="カテゴリーを入力" value='${category}'>
		<input type="submit" value="検索">
	</form>
	<br>

	<form action='index.jsp?action=searchDate' method='post'>
	日付 :
	<select name='year'>
		<c:forEach var="i" begin="2000" end="${year }" step="1">
			<option value="${i }" <c:if test="${year==i }">selected</c:if>>${i }</option>
		</c:forEach>
	</select>年

	<select name='month'>
		<c:forEach var="i" begin="0" end="12" step="1">
			<option value="${i }" <c:if test="${month==i }">selected</c:if>>${i }</option>
		</c:forEach>
	</select>月

	<select name='day'>
		<c:forEach var='i' begin='0' end='31' step='1'>
			<option value="${i }" <c:if test="${day==i }">selected</c:if>>${i }</option>
		</c:forEach>
	</select>日

	<input type='submit' value='検索'>
	</form><br>
	<form action='index.jsp?action=reset' method='post'>
		<input type='submit' value='検索リセット'>
	</form>
	<br>
	<br>
	<p>${message}</p>
	<br>
	<c:forEach var="post" items="${homePosts}">
		<div class='post'>
			<c:if test='${post.userId == loginAccount.userId }'>
				<form action='index.jsp?action=deletePost&id=${post.postId}'
					method="post">
					<input type='submit' value='削除'>
				</form>
				<br>
			</c:if>
			ユーザー名 : ${post.userName}<br> 件名 : ${post.subject}<br> 本文 :
			${post.content }<br> カテゴリー : ${post.category}<br> 日付 :
			${post.postedAt }<br> <br> <br>
			<form action='index.jsp?action=comment&id=${post.postId }'
				method='post'>
				<textarea name='comment'>${post.comment }</textarea>
				<input type='submit' value='送信'><br>
			</form>
			<br> <br>
		</div>
		<div class='comments'>
			<c:forEach var='comment' items='${post.commentList }'>
				<br>
				<c:if test='${loginAccount.userId == comment.userId }'>
					<form action='index.jsp?action=deleteComment&id=${comment.commentId }' method="post">
						<input type='submit' value='削除'>
					</form>
				</c:if>
				<p>ユーザー名 : ${comment.userName }</p>
				<p>日付 : ${comment.commentedAt}</p>
				<p>コメント : ${comment.comment }</p>
				<br>
				<br>
			</c:forEach>
		</div>
	</c:forEach>

</body>
</html>